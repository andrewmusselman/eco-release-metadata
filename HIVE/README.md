
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
* Apache Hive v2.2.0
    * [Changes](2.2.0/CHANGES.2.2.0.md)
    * [Release Notes](2.2.0/RELEASENOTES.2.2.0.md)
* Apache Hive v2.1.1
    * [Changes](2.1.1/CHANGES.2.1.1.md)
    * [Release Notes](2.1.1/RELEASENOTES.2.1.1.md)
* Apache Hive v2.1.0
    * [Changes](2.1.0/CHANGES.2.1.0.md)
    * [Release Notes](2.1.0/RELEASENOTES.2.1.0.md)
* Apache Hive v2.0.2
    * [Changes](2.0.2/CHANGES.2.0.2.md)
    * [Release Notes](2.0.2/RELEASENOTES.2.0.2.md)
* Apache Hive v2.0.1
    * [Changes](2.0.1/CHANGES.2.0.1.md)
    * [Release Notes](2.0.1/RELEASENOTES.2.0.1.md)
* Apache Hive v2.0.0
    * [Changes](2.0.0/CHANGES.2.0.0.md)
    * [Release Notes](2.0.0/RELEASENOTES.2.0.0.md)
* Apache Hive v2.00
    * [Changes](2.00/CHANGES.2.00.md)
    * [Release Notes](2.00/RELEASENOTES.2.00.md)
* Apache Hive v1.3.0
    * [Changes](1.3.0/CHANGES.1.3.0.md)
    * [Release Notes](1.3.0/RELEASENOTES.1.3.0.md)
* Apache Hive v1.2.2
    * [Changes](1.2.2/CHANGES.1.2.2.md)
    * [Release Notes](1.2.2/RELEASENOTES.1.2.2.md)
* Apache Hive v1.2.1
    * [Changes](1.2.1/CHANGES.1.2.1.md)
    * [Release Notes](1.2.1/RELEASENOTES.1.2.1.md)
* Apache Hive v1.2.0
    * [Changes](1.2.0/CHANGES.1.2.0.md)
    * [Release Notes](1.2.0/RELEASENOTES.1.2.0.md)
* Apache Hive v1.1.2
    * [Changes](1.1.2/CHANGES.1.1.2.md)
    * [Release Notes](1.1.2/RELEASENOTES.1.1.2.md)
* Apache Hive v1.1.1
    * [Changes](1.1.1/CHANGES.1.1.1.md)
    * [Release Notes](1.1.1/RELEASENOTES.1.1.1.md)
* Apache Hive v1.1.0
    * [Changes](1.1.0/CHANGES.1.1.0.md)
    * [Release Notes](1.1.0/RELEASENOTES.1.1.0.md)
* Apache Hive v1.0.1
    * [Changes](1.0.1/CHANGES.1.0.1.md)
    * [Release Notes](1.0.1/RELEASENOTES.1.0.1.md)
* Apache Hive v1.0.0
    * [Changes](1.0.0/CHANGES.1.0.0.md)
    * [Release Notes](1.0.0/RELEASENOTES.1.0.0.md)
* Apache Hive v0.15.0
    * [Changes](0.15.0/CHANGES.0.15.0.md)
    * [Release Notes](0.15.0/RELEASENOTES.0.15.0.md)
* Apache Hive v0.14.1
    * [Changes](0.14.1/CHANGES.0.14.1.md)
    * [Release Notes](0.14.1/RELEASENOTES.0.14.1.md)
* Apache Hive v0.14.0
    * [Changes](0.14.0/CHANGES.0.14.0.md)
    * [Release Notes](0.14.0/RELEASENOTES.0.14.0.md)
* Apache Hive v0.13.1
    * [Changes](0.13.1/CHANGES.0.13.1.md)
    * [Release Notes](0.13.1/RELEASENOTES.0.13.1.md)
* Apache Hive v0.13.0
    * [Changes](0.13.0/CHANGES.0.13.0.md)
    * [Release Notes](0.13.0/RELEASENOTES.0.13.0.md)
